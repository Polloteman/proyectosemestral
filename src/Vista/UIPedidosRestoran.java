//Felipe Serey Morales
package Vista;


import Controlador.*;
import Excepciones.*;
import java.io.IOException;
import java.util.Scanner;



public class UIPedidosRestoran {



    //Declaracion del Singleton de UIPedidosRestoran__________________________________
    private static UIPedidosRestoran instance =null;

    private UIPedidosRestoran(){

    }

    public static UIPedidosRestoran getInstancia(){
        if(instance == null){
            instance = new UIPedidosRestoran();
        }
        return instance;
    }

    //Declaracion del menu principal#################################################################################################
    public void menuprincipal() throws ObjetoRepetidoException, ProductoNoEncontradoException, ValorInvalidoException, ClienteNoEncontradoException, RepartidorNoEncontradoException, PedidoNoEncontradoException, OperacionInvalidaException, IOException {
        //Entrada de datos para opciones
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        boolean guardar = false;
        boolean cardaGuardaCompletado;


        int menu = 0;
        // Si menu es 10, entonces la interfaz se cierra
        while(menu != 10){
            // reinicia la variable de opciones
            menu = 0;

            //Proceso de cargado o guardado de archivos
            cardaGuardaCompletado = false;
            while(!cardaGuardaCompletado){
                if(guardar){
                    System.out.println("¿Desea guardar los datos?");
                    System.out.println("1- Si");
                    System.out.println("2- No");
                    int a = scanner.nextInt();
                    switch (a) {
                        case 1:
                            ControladorPedidosRestoran.getInstancia().writeDatosSistema();
                            cardaGuardaCompletado = true;
                            break;
                        case 2:
                            cardaGuardaCompletado = true;
                            break;
                        default:
                            System.out.println("Porfavor escriba un numero valido");
                            break;
                    }
                }else{
                    System.out.println("¿Desea leer datos antiguos?");
                    System.out.println("1- Si");
                    System.out.println("2- No");
                    int a = scanner.nextInt();

                    switch (a){
                        case 1:
                            ControladorPedidosRestoran.getInstancia().readDatosSistema();
                            guardar = true;
                            cardaGuardaCompletado = true;
                            break;
                        case 2:
                            guardar = true;
                            cardaGuardaCompletado = true;
                            break;
                        default:
                            System.out.println("Porfavor escriba un numero valido");

                            break;
                    }

                }
            }

            System.out.println("..........................................................");
            System.out.println("...........:::SISTEMA DE PEDIDOS DE RESTORÁN:::...........");
            System.out.println();
            System.out.println("...::: MENÚ PRINCIPAL :::...");
            System.out.println("1. Crear cliente/repartidor");
            System.out.println("2. Crear producto");
            System.out.println("3. Ingresar Pedido");
            System.out.println("4. Ingresar entrega pedido");
            System.out.println("5. Listar productos");
            System.out.println("6. Listar repartidores");
            System.out.println("7. Listar pedidos pendientes");
            System.out.println("8. Listar pedidos por entregar");
            System.out.println("9. Listar pedidos más solicitados");
            System.out.println("10. Salir");
            System.out.println();
            System.out.print("    Ingrese opción:  ");

            // Selecciona una opcion a partir del usuario
            if (scanner.hasNextInt()){
                menu = scanner.nextInt();
                switch(menu){
                    case 1:
                        creaClienteRepartidor();
                        break;
                    case 2:
                        crearProducto();
                        break;
                    case 3:
                        ingresaPedidoCliente();
                        break;
                    case 4:
                        ingresaEntregaPedido();
                        break;
                    case 5:
                        listaProductos();
                        break;
                    case 6:
                        listaRepartidores();
                        break;
                    case 7:
                        listaPedidosPendientes();
                        break;
                    case 8:
                        listaPedidosPorEntegar();
                        break;
                    case 9:
                        listaPedidosMasSolicitados();
                        break;
                    case 10:
                        System.out.print("Saliendo........");
                        break;
                    default:
                        System.out.println("Error, porfavor, ingrese un numero menor que 11 o mayor a 0");
                        System.out.println("<Pulse  Enter Para continuar>");
                        scanner.nextLine();
                        break;
                }

            }
            else{
                System.out.println("Error, porfavor, ingrese un numero valido");
                System.out.println("<Pulse  Enter Para continuar>");
                scanner.nextLine();
            }
        }
    }

    //Metodos secundarios ----------------------------------------------------------------------------------------------------------
    public void creaClienteRepartidor(){
        GuiCreaClienteRepartidor test = new GuiCreaClienteRepartidor();

    }
    public void crearProducto() throws ObjetoRepetidoException, ValorInvalidoException, ProductoNoEncontradoException {

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        int codigo;
        String nombre;
        int precio;
        int margen;
        int stock;

        System.out.println("***CREACIÓN DE UN NUEVO PRODUCTO***");
        System.out.println("...:::MENÚ:::...");
        System.out.println("1. Crear producto básico");
        System.out.println("2. Crear combo");
        System.out.println("    Ingrese opción");
        if(scanner.hasNextInt()){
            switch (scanner.nextInt()){
                case 1:
                    System.out.print("Codigo: ");
                    codigo = scanner.nextInt();


                    System.out.print("Nombre: ");
                    nombre = scanner.next();


                    System.out.print("Precio de costo:  ");
                    precio = scanner.nextInt();


                    System.out.print("Margen de Venta:  ");
                    margen = scanner.nextInt();

                    System.out.print("Stock inicial:  ");
                    stock = scanner.nextInt();

                    ControladorPedidosRestoran.getInstancia().addProductoBasico(codigo,nombre,precio,margen);
                    ControladorPedidosRestoran.getInstancia().addStockAProducto(codigo,stock);
                    break;
                case 2:
                    System.out.print("Codigo: ");
                    codigo = scanner.nextInt();


                    System.out.print("Nombre: ");
                    nombre = scanner.next();


                    System.out.print("Margen de Venta:  ");
                    margen = scanner.nextInt();

                    System.out.print("Stock inicial:  ");
                    stock = scanner.nextInt();

                    ControladorPedidosRestoran.getInstancia().addCombo(codigo,nombre,margen);
                    ControladorPedidosRestoran.getInstancia().addStockAProducto(codigo,stock);
                    break;
                default:
                    System.out.println("ERROR se encontro un numero no valido");
                    break;
            }
        }else{
            System.out.println("ERROR no se ha encontrado un numero");
        }

        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();

    }
    public void ingresaPedidoCliente() throws ClienteNoEncontradoException, ProductoNoEncontradoException, RepartidorNoEncontradoException, PedidoNoEncontradoException {

        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        String[] cliente;
        int pedidostotales = 0;
        int distintos;
        String rutCliente;
        String repartidor;

        int[][] datosProducto;
        String[] producto;
        System.out.println("***NUEVO PEDIDO***");

        System.out.print("Rut de cliente: ");
        rutCliente = scanner.next();
        cliente = ControladorPedidosRestoran.getInstancia().getDatosCliente(rutCliente);
        System.out.println("Nombre cliente: "+ cliente[1]);


        System.out.print("ingrese el numero de objetos distintos que desea incluir: ");
        distintos = scanner.nextInt();



        for(int i = 0; i < distintos; i++){
            datosProducto = new int[1][2];
            System.out.print("Codigo: "  );
            datosProducto[0][0] = scanner.nextInt();
            producto = ControladorPedidosRestoran.getInstancia().getDatosProducto(datosProducto[0][0]);

            System.out.println("Nombre producto: "+ producto[1]);

            System.out.print("Cantidad:   ");
            datosProducto[0][1]= scanner.nextInt();
            ControladorPedidosRestoran.getInstancia().addPedido(rutCliente,datosProducto);

            if(i == 0){
                String[][] a = ControladorPedidosRestoran.getInstancia().listPedidosPendientes(rutCliente);
                for(String[] A : a){
                    Scanner b = new Scanner(A[0]);
                    int c = b.nextInt();
                    if(c  > pedidostotales){
                        pedidostotales = c;
                    }
                }
            }
            System.out.println("El codigo del pedido es: " + pedidostotales);
            System.out.println("Ingrese el rut del repartidor: "  );
            repartidor = scanner.next();
            ControladorPedidosRestoran.getInstancia().setRepartidorAPedido(repartidor,pedidostotales);
            pedidostotales += 1;
        }
        System.out.println("¿Desea generar un comprobante?");
        System.out.println("Escriba 1 para continuar");
        if(scanner.hasNext()){
            if(scanner.nextInt()== 1){

            }
        }


        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();

    }
    public void ingresaEntregaPedido() throws PedidoNoEncontradoException, OperacionInvalidaException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        int codigo;

        System.out.println("***ENTREGA DE PEDIDO***");
        System.out.print("Ingrese codigo de Producto:   ");
        codigo = scanner.nextInt();
        ControladorPedidosRestoran.getInstancia().setEntregaPedido(codigo);
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();

    }
    public void listaProductos(){
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");
        System.out.println("***LISTADO DE PRODUCTOS***" );
        creaListas(
                new String[]{"Codigo","Nombre" ,"Costo","Margen","Stock"},
                new boolean[]{true,false,true,true,true},
                new  int[]{1,7,5,5},
                ControladorPedidosRestoran.getInstancia().listProductos()
                );
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();

    }
    public void listaRepartidores(){
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        System.out.println("***LISTADO DE REPARTIDORES***" );
        creaListas(
                new String[]{"Rut","Nombre" ,"Domicilio","Telefono"},
                new boolean[]{false,false,false,false},
                new  int[]{3,5,4},
                ControladorPedidosRestoran.getInstancia().listRepartidores()
        );
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();
    }
    public void listaPedidosPendientes() throws ClienteNoEncontradoException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");
        String rutCliente;
        System.out.print("Rut");
        rutCliente = scanner.next();
        System.out.println("***LISTADO DE PEDIDOS PENDIENTES DE UN CLIENTE***" );
        creaListas(
                new String[]{"Numero","Fecha" ,"Rut repartidor","Nombre", "Repartidor"},
                new boolean[]{true,false,true,false,false},
                new  int[]{1,1,1,1},
                ControladorPedidosRestoran.getInstancia().listPedidosPendientes(rutCliente)
        );
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();
    }
    public void listaPedidosPorEntegar() throws RepartidorNoEncontradoException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");

        String rutRepartidor;
        System.out.print("Rut de cliente: ");
        rutRepartidor = scanner.next();
        System.out.println("***LISTADO DE PEDIDOS POR ENTREGAR DE UN REPARTIDOR***" );
        creaListas(
                new String[]{"Numero","Fecha" ,"Rut cliente","Nombre", "cliente"},
                new boolean[]{true,false,false,false,false},
                new  int[]{1,1,1,1},
                ControladorPedidosRestoran.getInstancia().listPedidosPorEntregar(rutRepartidor)
        );
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();
    }
    public void listaPedidosMasSolicitados() throws ValorInvalidoException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");
        int nroMinPedidos;
        System.out.print("Numero minimo de pedidos: ");
        nroMinPedidos = scanner.nextInt();
        System.out.println("***LISTADO DE PEDIDOS POR ENTREGAR DE UN REPARTIDOR***" );
        creaListas(
                new String[]{"Numero","Fecha" ,"Rut cliente","Nombre", "cliente"},
                new boolean[]{true,false,false,false,false},
                new  int[]{1,1,1,1},
                ControladorPedidosRestoran.getInstancia().listProductosMasSolicitados(nroMinPedidos)
        );
        System.out.println("<Pulse  Enter Para continuar>");
        scanner.nextLine();
    }
    public void creaDatosDePrueba(){


    }
    private void creaListas(String[] titulos, boolean[] posicion, int[] espacios, String[][] elementos ){
        char[] linea;
        int[] sublargo = new int[elementos[0].length];
        int largototal = 0;
        int a;
        int b;
        int c;

        for(int i = 0;i< titulos.length; i++){
            sublargo[i] = titulos[i].length();
        }
        for (String[] strings : elementos) {
            for (int ii = 0; ii < strings.length; ii++) {
                if (sublargo[ii] < strings[ii].length()) {
                    sublargo[ii] = strings[ii].length();
                }
            }
        }
        for (int j : sublargo) {
            largototal += j;

        }
        for (int espacio : espacios) {
            largototal += espacio;

        }

         a = 0;
         b = 0;
         c = 0;
         linea = new char[largototal];
        for(int i = 0; i < largototal; i++ ){
            if(c >= titulos[a].length() && a < titulos.length-1 ){
                c = 0;
                b = b + sublargo[a] + espacios[a];
                a++;

            }

            if(posicion[a]){
                if(b+sublargo[a]-titulos[a].length() <= i && i < b + sublargo[a]){
                    linea[i] =  titulos[a].charAt(c);
                    c++;
                }
            }else{
                if(b<= i && i < b + titulos[a].length()){
                    linea[i] =  titulos[a].charAt(c);
                    c++;
                }
            }
            if(linea[i]== 0){
                linea[i]= ' ';
            }
        }
        System.out.println(linea);

        linea = new char[largototal];
        for(int i = 0; i < largototal; i++ ){
            linea[i] = '-';
        }
        System.out.println(linea);

        for(String[] elemento:elementos){
            a = 0;
            b = 0;
            c = 0;
            linea = new char[largototal];
            for(int i = 0; i < largototal; i++ ){
                if(c >= elemento[a].length() && a < elemento.length-1 ) {
                    c = 0;
                    b = b + sublargo[a] + espacios[a];
                    a++;

                }
                if(posicion[a]){
                    if(b+sublargo[a]-elemento[a].length() <=i && i < b + sublargo[a]){
                        linea[i] =  elemento[a].charAt(c);
                        c++;
                    }
                }else{
                    if(b<= i && i < b + elemento[a].length()){
                        linea[i] =  elemento[a].charAt(c);
                        c++;
                    }
                }
                if(linea[i]== 0){
                    linea[i]= ' ';
                }
            }
            System.out.println(linea);
        }

    }

}
