package Vista;

import javax.swing.*;
import java.awt.event.*;
import Controlador.*;
import Excepciones.*;

public class GuiCreaClienteRepartidor extends JFrame implements ActionListener {
    JFrame frame = new JFrame("test");
    JLabel rutLabel;
    JLabel nombreLabel;
    JLabel domicilioLabel;
    JLabel telefonoLabel;
    JTextField rutTextField;
    JTextField nombreTextField;
    JTextField domicilioTextField;
    JTextField telefonoTextField;
    ButtonGroup persona = new ButtonGroup();
    JRadioButton clienteRadioButton;
    JRadioButton repartidorRadioButton;
    JButton aceptar;
    JButton cancelar;

    public GuiCreaClienteRepartidor(){
        int columnas = 10;



        frame.setLayout(null);
        frame.setBounds(0,0,300,300);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


        //Primera Linea: Rut

        rutLabel = new JLabel("Rut");
        rutLabel.setBounds(10,columnas,100,20);
        frame.add(rutLabel);

        rutTextField = new JTextField();
        rutTextField.setBounds(110,columnas,100,20);
        frame.add(rutTextField);

        //Segunda linea: Nombre y radioButton del Cliente
        columnas +=30;

        nombreLabel = new JLabel("Nombre");
        nombreLabel.setBounds(10,columnas,100,20);
        frame.add(nombreLabel);

        nombreTextField = new JTextField();
        nombreTextField.setBounds(110,columnas,100,20);
        frame.add(nombreTextField);

        clienteRadioButton = new JRadioButton("Cliente");
        clienteRadioButton.setBounds(210,columnas,100,20);
        persona.add(clienteRadioButton);
        frame.add(clienteRadioButton);

        //Tercera linea: Domicilio y radioButton del Repartidor
        columnas +=30;

        domicilioLabel = new JLabel("Domicilio");
        domicilioLabel.setBounds(10,columnas,100,20);
        frame.add(domicilioLabel);

        domicilioTextField = new JTextField();
        domicilioTextField.setBounds(110,columnas,100,20);
        frame.add(domicilioTextField);

        repartidorRadioButton = new JRadioButton("Repartidor");
        repartidorRadioButton.setBounds(210,columnas,100,20);
        persona.add(repartidorRadioButton);
        frame.add(repartidorRadioButton);

        //Cuarta linea: Telefono
        columnas +=30;

        telefonoLabel = new JLabel("Telefono");
        telefonoLabel.setBounds(10,columnas,100,20);
        frame.add(telefonoLabel);

        telefonoTextField = new JTextField();
        telefonoTextField.setBounds(110,columnas,100,20);
        frame.add(telefonoTextField);

        //Quinta linea: Botones de aceptar y cancelar
        columnas +=30;


        aceptar = new JButton("Aceptar");
        aceptar.setBounds(10,columnas,100,20);

        frame.add(aceptar);

        cancelar = new JButton("Cancelar");
        cancelar.setBounds(110,columnas,100,20);

        frame.add(cancelar);
        frame.setVisible(true);
        aceptar.addActionListener(this);
        cancelar.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent event){
        if(event.getSource()== aceptar){
            if(clienteRadioButton.isSelected()){
                String rut = rutTextField.getText();
                if(rut.equals("")){
                    JOptionPane.showMessageDialog(null,"Rut no valido","Resultado de transaccion",JOptionPane.ERROR_MESSAGE);
                }
                try {
                    ControladorPedidosRestoran.getInstancia().addCliente(
                            rutTextField.getText(),
                            nombreTextField.getText(),
                            domicilioTextField.getText(),
                            telefonoTextField.getText()
                    );
                    JOptionPane.showMessageDialog(null,"Datos ingresados con exito","Resultado de transaccion",JOptionPane.INFORMATION_MESSAGE);
                    frame.setVisible(false);
                    frame.dispose();

                }catch (ObjetoRepetidoException e) {
                    JOptionPane.showMessageDialog(null,"ya existe un cliente con el rut dado","Resultado de transaccion",JOptionPane.ERROR_MESSAGE);

                }

            }else if(repartidorRadioButton.isSelected()){
                String rut = rutTextField.getText();
                if(rut.equals("")){
                    JOptionPane.showMessageDialog(null,"Rut no valido","Resultado de transaccion",JOptionPane.ERROR_MESSAGE);
                }
                try {
                    ControladorPedidosRestoran.getInstancia().addRepartidor(
                            rutTextField.getText(),
                            nombreTextField.getText(),
                            domicilioTextField.getText(),
                            telefonoTextField.getText()
                    );
                    JOptionPane.showMessageDialog(null,"Datos ingresados con exito","Resultado de transaccion",JOptionPane.INFORMATION_MESSAGE);
                    frame.setVisible(false);
                    frame.dispose();
                }catch (ObjetoRepetidoException e) {
                    JOptionPane.showMessageDialog(null,"Ya existe un repartidor con el rut dado","Resultado de transaccion",JOptionPane.ERROR_MESSAGE);
            }

        }else {
                JOptionPane.showMessageDialog(null,"Debe seleccionar cliente o repartidor","Resultado de transaccion",JOptionPane.INFORMATION_MESSAGE);
            }
        }
        if(event.getSource()== cancelar){
            frame.setVisible(false);
            frame.dispose();
        }
    }

}
