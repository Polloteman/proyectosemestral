//Felipe Serey Morales
package Vista;

import Excepciones.*;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws ProductoNoEncontradoException, RepartidorNoEncontradoException, PedidoNoEncontradoException, ObjetoRepetidoException, ClienteNoEncontradoException, ValorInvalidoException, OperacionInvalidaException, IOException {
        UIPedidosRestoran.getInstancia().menuprincipal();
    }
}
