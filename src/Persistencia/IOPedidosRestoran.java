package Persistencia;

import Modelo.*;


import java.io.*;
import java.util.ArrayList;



public class IOPedidosRestoran implements Serializable {
    private static IOPedidosRestoran instance = null;
    private Repartidor repartidor;
    private IOPedidosRestoran(){
    }
    public static IOPedidosRestoran getInstance(){
        if(instance==null){
            instance = new IOPedidosRestoran();
        }
        return instance;
    }
    public Producto[] readProductosSinPedido() throws IOException, ClassNotFoundException {
        ArrayList<Producto> prod = new ArrayList<>();
        ObjectInputStream objprod = new ObjectInputStream(new FileInputStream("productos.obj"));
        try {
            while (true){
                prod.add((Producto) objprod.readObject());
            }

        }catch (EOFException e){
            objprod.close();
        }
        return prod.toArray(new Producto[0]);
    }
    public Cliente[] readClientesSinPedido() throws IOException, ClassNotFoundException {
        ArrayList<Cliente> cli = new ArrayList<>();
        ObjectInputStream objCli = new ObjectInputStream(new FileInputStream("clientes.obj"));
        try {
            while (true){
                cli.add((Cliente) objCli.readObject());
            }

        } catch (EOFException e) {
            objCli.close();

        }
        return cli.toArray(new Cliente[0]);
    }
    public Repartidor[] readRepartidoresSinPedido() throws IOException, ClassNotFoundException {
        ArrayList<Repartidor> Rep = new ArrayList<>();
        ObjectInputStream objRep = new ObjectInputStream(new FileInputStream("repartidor.obj"));
        try {
            while(true){
                Rep.add((Repartidor) objRep.readObject());
            }
        } catch (EOFException e) {
            objRep.close();

        }
        return Rep.toArray(new Repartidor[0]);
    }
    public Pedido[] readPedidos() throws IOException, ClassNotFoundException {
        ArrayList<Pedido> Ped = new ArrayList<>();
        ObjectInputStream objPed = new ObjectInputStream(new FileInputStream("Pedidos.obj"));
        try {
            while(true){
                Ped.add((Pedido) objPed.readObject());
            }

        } catch (EOFException e) {
            objPed.close();

        }
        return Ped.toArray(new Pedido[0]);
    }
    public void saveProductosYPersonasSinPedidos (Producto[] prod, Cliente[] cli, Repartidor[] rep) throws  IOException {
        ObjectOutputStream objOut;
         objOut  = new ObjectOutputStream(new FileOutputStream("productos.obj"));
        for(Producto producto: prod){
            if(producto.getNroLineasPedidos() == 0){
                objOut.writeObject(producto);
            }

        }
        objOut.close();

         objOut  = new ObjectOutputStream(new FileOutputStream("clientes.obj"));
        for(Cliente cliente : cli){
            if(cliente.getNroPedidos() == 0){
                objOut.writeObject(cliente);
            }

        }
        objOut.close();
         objOut  = new ObjectOutputStream(new FileOutputStream("repartidor.obj"));
        for (Repartidor repartidor : rep) {
            if (repartidor.getNroPedidos() == 0) {
                objOut.writeObject(repartidor);
            }
        }
        objOut.close();
    }
    public void savePedidos(Pedido[] pedidos) throws IOException{
        ObjectOutputStream objOut  = new ObjectOutputStream(new FileOutputStream("Pedidos.obj"));
        for(Pedido pedido:pedidos){
            objOut.writeObject(pedido);
        }
        objOut.close();
    }
    public void saveComprobantePedido(Pedido pedido) throws FileNotFoundException {
        PrintWriter recibo = new PrintWriter(new FileOutputStream("Pedido"+ pedido.getNumero() +".txt"));
        char[] linea;
        int[] sublinea = new int[4];
        int[] linealargo = new int[5];
        int[] espacios = new int[]{1,1,1,1};
        int linealargomaximo = 40;
        LineaPedido[] lineaPedido = pedido.getLineasPedido();
        String[][] a= new String[pedido.getLineasPedido().length][5];
        for(int i = 0; i < a.length; i++){
           a[i][0] = String.valueOf(i);
           if(linealargo[0] < a[i][0].length()){
               linealargo[0] = a[i][0].length();
           }
           a[i][1] = lineaPedido[i].getProducto().getNombre();
            if(linealargo[1] < a[i][1].length()){
                linealargo[1] = a[i][1].length();
            }
           a[i][2] = String.valueOf(lineaPedido[i].getSubtotal()/lineaPedido[i].getProducto().getPrecioVenta());
            if(linealargo[2] < a[i][2].length()){
                linealargo[2] = a[i][2].length();
            }
           a[i][3] = String.valueOf(lineaPedido[i].getProducto().getPrecioVenta());
            if(linealargo[3] < a[i][3].length()){
                linealargo[3] = a[i][3].length();
            }
           a[i][4] = String.valueOf(lineaPedido[i].getSubtotal());
            if(linealargo[4] < a[i][4].length()){
                linealargo[4] = a[i][4].length();
            }
        }
        linealargomaximo += linealargo[0];
        linealargomaximo += linealargo[1];
        linealargomaximo += linealargo[2];
        linealargomaximo += linealargo[3];
        linealargomaximo += linealargo[4];
        linealargomaximo += espacios[0];
        linealargomaximo += espacios[1];
        linealargomaximo += espacios[2];
        linealargomaximo += espacios[3];

        sublinea[0] += linealargo[0];
        sublinea[1] += linealargo[1] + espacios[0];
        sublinea[2] += linealargo[2] + espacios[1];
        sublinea[3] += linealargo[3] + espacios[2];

        recibo.println("        PEDIDO No°"+pedido.getNumero());
        recibo.println("Fecha     :"+pedido.getFecha());
        recibo.println("Cliente   :"+pedido.getCliente());
        recibo.println("--------DETALLES PRODUCTOS-------------------------------");



        for(int i = 0; i < a.length; i++){
            linea = new char[linealargomaximo];
            for(int i1 = 0; i1 < linealargomaximo; i1++){
                if(sublinea[0] - a[i][0].length() <= i1 && i1 <= sublinea[0]){
                    linea[i1] = a[i][0].toCharArray()[i1-(sublinea[0] - a[i][0].length())];
                }
                if(sublinea[0] + espacios[0] <= i1 && i1 <= sublinea[0] + espacios[0]+a[i][1].length()){
                    linea[i1] = a[i][1].toCharArray()[i1-(sublinea[1] - a[i][1].length())];
                }
                if(sublinea[2] - a[i][2].length() <= i1 && i1 <= sublinea[2]){
                    linea[i1] = a[i][2].toCharArray()[i1-(sublinea[2] - a[i][2].length())];
                }
                if(sublinea[3] - a[i][3].length() <= i1 && i1 <= sublinea[3]){
                    linea[i1] = a[i][3].toCharArray()[i1-(sublinea[3] - a[i][3].length())];
                }
                if(linealargomaximo - a[i][4].length() <= i1){
                    linea[i1] = a[i][4].toCharArray()[i1-(linealargomaximo - a[i][4].length())];
                }
            }
            recibo.println(linea);
        }
        recibo.println("-----------------------------------------------------------");
        recibo.println("TOTAL$" +pedido.getMontoTotal());
        recibo.println("-----------------------------------------------------------");
        recibo.close();

    }
}
