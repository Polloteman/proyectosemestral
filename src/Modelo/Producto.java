//Felipe Serey Morales
package Modelo;


import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;


public abstract class   Producto implements Serializable {
    //atributos-----------------------------------
    private final int codigo;
    private final String nombre;
    private final int margenVentas;
    private int stock;
    //asociaciones--------------------------------

    private final ArrayList<LineaPedido> lineaPedidos;

    //Constructor------------------------------------
    public Producto (int cod, String nom, int costo, int margen){
        codigo = cod;
        nombre = nom;
        margenVentas = margen;
        stock = 0;
        lineaPedidos = new ArrayList<>();
    }
    //Metodos----------------------------------------------
    public int getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public abstract int getPrecioCosto();
    public int getPrecioVenta(){
        return getPrecioCosto() + margenVentas;
    }
    public void addStock(int nroUnidades)throws ValorInvalidoException {
        if(nroUnidades <= 0){
            throw new ValorInvalidoException("No se puede añadir un Stock negativo");
        }
        stock += nroUnidades;
    }
    public boolean removeStock(int nroUnidades)throws ValorInvalidoException{
        if(nroUnidades <= 0){
            throw new ValorInvalidoException("No se puede quitar un Stock negativo");
        }
        if(stock < nroUnidades){
            throw new ValorInvalidoException("Se esta quitando mas unidades de las existentes");
        }
        stock -= nroUnidades; return true;

    }
    public void addLineaPedidos(LineaPedido linea){
        lineaPedidos.add(linea);
    }
    public int getNroLineasPedidos(){
        return lineaPedidos.size();
    }
    public void addProducto(Producto producto){}
    public Producto getProducto(int pos){
        return null;
    }
    public int getNroProductos(){
        return 0;
    }

    public boolean equals(Producto other) {
        return this.getNombre().equals(other.getNombre());
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, nombre, margenVentas, stock, lineaPedidos);
    }

    @Override
    public String toString(){return String.valueOf(codigo) + '/'+ nombre +'/'+ margenVentas + '/' + stock;}
}
