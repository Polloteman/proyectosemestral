package Modelo;

import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;

public abstract class Persona implements Serializable {
    //Atributos---------------------------------------------
    private final String rut;
    private final String nombre;
    private final String direccion;
    private final String telefono;
    //Relaciones---------------------------------------


    public Persona(String rut, String nom, String dir, String tel){
        this.rut = rut;
        nombre = nom;
        direccion = dir;
        telefono = tel;
    }
    public String getRut(){
        return rut;
    }
    public String getNombre(){
        return nombre;
    }
    public boolean validaRut(String rut){
        return this.rut.equals(rut);
    }
    public abstract void addPedido(Pedido pedido) throws ObjetoRepetidoException, ValorInvalidoException;
    public abstract Pedido[] getPedidosPendientes();
    public abstract int getNroPedidos();

    public boolean equals(Persona other){
        return this.getRut().equals(other.getRut());
    }
    @Override
    public String toString(){
        return rut+'/'+ nombre + '/'+direccion + '/'+ telefono;
    }
}
