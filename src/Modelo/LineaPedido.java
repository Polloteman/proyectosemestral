//Felipe Serey Morales
package Modelo;

import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.Objects;



public class LineaPedido implements Serializable {
    //Atrubutos---------------------------------
    private final int cantidad;
    //Relaciones-----------------------------------
    private final Pedido pedidos;
    private final Producto productos;
    //Constructor-------------------------------------
    public LineaPedido(Pedido pedido, Producto producto, int cantidad)throws ValorInvalidoException {
        if(cantidad < 0){
            throw new ValorInvalidoException("Cantidad es < que 0");
        }
        this.cantidad = cantidad;
        pedidos = pedido;
        productos = producto;
    }
    //Metodos------------------------------------------------
    public int getSubtotal(){
        return productos.getPrecioVenta()*cantidad;
    }
    public Producto getProducto(){
        return productos;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LineaPedido)) return false;
        LineaPedido that = (LineaPedido) o;
        return cantidad == that.cantidad && Objects.equals(pedidos, that.pedidos) && Objects.equals(productos, that.productos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cantidad, pedidos, productos);
    }

    @Override
    public String toString(){
        return String.valueOf(cantidad) +'/'+ pedidos.getNumero() +'/'+ productos.getCodigo();
    }

}
