//Felipe Serey Morales
package Modelo;

import Excepciones.OperacionInvalidaException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;


public class Pedido implements Serializable {
    public enum Estado {
        PENDIENTE,
        ENTREGADO
    }

    //atrubutos--------------------------------------
             private final int numero;
             private final LocalDate fecha;
             private  Estado estado;
    //Relaciones--------------------------------------------
    private final Cliente clientes;
    private Repartidor repartidores;
    private final ArrayList<LineaPedido> lineaPedidos;
    private final ArrayList<Producto> productos ;
    //Constructor------------------------------------------
    public Pedido(int nro, LocalDate fecha,Cliente cliente ){
        numero = nro;
        this.fecha = fecha;
        estado = Estado.PENDIENTE;
        clientes = cliente;
        repartidores =null;
        lineaPedidos = new ArrayList<>();
        productos = new ArrayList<>();
    }
    //Metodos----------------------------------------------
    public int getNumero(){
        return  numero;
    }
    public  LocalDate getFecha(){
        return fecha;
    }
    public Estado getEstado(){
        return estado;
    }
    public Cliente getCliente(){
        return clientes;
    }
    public Repartidor getRepartidor(){
        return repartidores;

    }
    public void addProducto(Producto producto,int cantidad)throws ValorInvalidoException, OperacionInvalidaException {
        int a;
        Scanner b = new Scanner(producto.toString());
        b.useDelimiter("/");
        b.next();
        b.next();
        b.next();
        a = b.nextInt();
        if(cantidad < 0){
            throw new ValorInvalidoException("El stock del producto es negativo");
        }
        if(a < cantidad){
            throw new ValorInvalidoException("El stock del producto es insuficiente");
        }
        if(this.estado == Estado.ENTREGADO) {
            throw new OperacionInvalidaException("El pedido ya ha sido entregado y no se puede modificar");
        }
        productos.add(producto);
        producto.removeStock(cantidad);
        lineaPedidos.add(new LineaPedido(this, producto,cantidad));
    }
    public void setrepartidor(Repartidor repartidor){
        repartidores = repartidor;
    }
    public int getMontoTotal(){
        int a =0;
        for (Producto producto :productos){
            a+= producto.getPrecioVenta();
        }
        return a;
    }
    public void setEntregado(){
        estado = Estado.ENTREGADO;
    }
    public LineaPedido[] getLineasPedido(){
        LineaPedido[] a = new LineaPedido[lineaPedidos.size()];
        return lineaPedidos.toArray(a);
    }

}
