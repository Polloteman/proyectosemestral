//Felipe Serey Morales
package Modelo;


import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;

public class Cliente extends Persona implements Serializable {
    //atributos------------------------------------------------------

    //Relaciones------------------------------------------------------
    private final ArrayList<Pedido> pedidos;
    //Constructor-------------------------------------------------------
    public Cliente(String rut,String nom,String dom,String tel){
        super(rut,nom,dom,tel);
        pedidos = new ArrayList<>();
    }
    //Metodos-----------------------------------------------------------

    @Override
    public void addPedido(Pedido pedido) throws ObjetoRepetidoException, ValorInvalidoException {
        for(Pedido pedido1 : pedidos){
            if(pedido1.getNumero() == pedido.getNumero()){
                throw new ObjetoRepetidoException("Ya existe un pedido con ese codigo");
            }
            pedidos.add(pedido);
        }
        if(!this.equals(pedido.getCliente())){
            throw new ValorInvalidoException("El cliente registrado en el pedido y el cliente receptor no son los mismos");
        }




    }
    @Override
    public Pedido[] getPedidosPendientes(){
        ArrayList<Pedido> a = new ArrayList<>();
        Pedido[] b;
        for(Pedido pedido: pedidos){
            if(pedido.getEstado() == Pedido.Estado.PENDIENTE){
                a.add(pedido);
            }
        }
        b = new Pedido[a.size()];
        b = a.toArray(b);
        for(int i =0 ; i < b.length; i++){
            b[i] = new Pedido(a.get(i).getNumero(),a.get(i).getFecha(),a.get(i).getCliente());
            b[i].setrepartidor(a.get(i).getRepartidor());
        }
        return b;
    }
    @Override
    public int getNroPedidos(){
        return pedidos.size();

    }


}
