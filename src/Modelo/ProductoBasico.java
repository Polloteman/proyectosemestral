package Modelo;

import java.io.Serializable;

public class ProductoBasico extends Producto implements Serializable {
    int precioCosto;
    public ProductoBasico (int cod, String nombre,int costo, int margen){
        super(cod,nombre,costo,margen);
        precioCosto = costo;
    }
    @Override
    public int getPrecioCosto(){
        return precioCosto;
    }


}
