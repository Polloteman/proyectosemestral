//Felipe Serey Morales
package Modelo;


import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;


public class Repartidor extends Persona implements Serializable {
    //Relaciones------------------------------------------------------
    private final ArrayList<Pedido> pedidos;

    //constructor-----------------------------------------------
    public Repartidor(String rut, String nom, String dom, String tel) {
        super(rut, nom, dom, tel);
        pedidos = new ArrayList<>();
    }

    //Metodos------------------------------------------------
    @Override
    public void addPedido(Pedido pedido) throws ObjetoRepetidoException, ValorInvalidoException {
        for (Pedido pedido1 : pedidos) {
            if (pedido1.getNumero() == pedido.getNumero()) {
                throw new ObjetoRepetidoException("ya existe un pedido registrado con ese codigo");
            }
        }
        if (!this.equals(pedido.getRepartidor())) {
            throw new ValorInvalidoException("los repartidores en el Pedido no coinciden");
        }
        pedidos.add(pedido);
    }
    @Override
    public Pedido[] getPedidosPendientes() {
        ArrayList<Pedido> a = new ArrayList<>();
        Pedido[] b;
        for (Pedido pedido : pedidos) {
            if (pedido.getEstado() == Pedido.Estado.PENDIENTE) {
                a.add(pedido);
            }
        }
        b = new Pedido[a.size()];
        b = a.toArray(b);
        return b;
    }
    @Override
    public int getNroPedidos() {
        return pedidos.size();
    }
}