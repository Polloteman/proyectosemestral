package Modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class Combo extends Producto implements Serializable {
    //Relaciones---------------------------------------
    private final ArrayList<Producto> productos;
    public Combo(int cod, String nom, int margen){
        super(cod,nom,0,margen);
        productos = new ArrayList<>();
    }
    @Override
    public int getPrecioCosto(){
        int a = 0;
        for(Producto producto: productos){
            a += producto.getPrecioCosto();
        }
        return a;
    }
    @Override
    public void addProducto(Producto producto){
        productos.add(producto);
    }
    @Override
    public Producto getProducto(int pos){
        return productos.get(pos);
    }
    @Override
    public int getNroProductos(){
        return productos.size();
    }

}
