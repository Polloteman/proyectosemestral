//Felipe Serey Morales
package Controlador;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


import Excepciones.*;
import Modelo.*;
import Persistencia.IOPedidosRestoran;

public class ControladorPedidosRestoran {
    //Relaciones-----------------------------------------
    private final ArrayList<LineaPedido> lineaPedidos;
    private final ArrayList<Cliente> clientes;
    private final ArrayList<Repartidor> repartidores;
    private final ArrayList<Producto> productos;
    private final ArrayList<Pedido> pedidos;
    //declaracion del Singelton de "ControladorPedidosRestoran"-------
    private static ControladorPedidosRestoran instance =null;

    private ControladorPedidosRestoran(){
    clientes = new ArrayList<>();
    repartidores = new ArrayList<>();
    productos = new ArrayList<>();
    pedidos = new ArrayList<>();
    lineaPedidos = new ArrayList<>();
    }
    //Metodos----------------------------------------------------
    public static ControladorPedidosRestoran getInstancia(){
        if(instance==null){
            instance = new ControladorPedidosRestoran();
        }
        return instance;
    }
    public int addPedido(String rutCliente,int[][] datosProductos )throws ClienteNoEncontradoException, ProductoNoEncontradoException {

        int a = -1;
        int b = -1;
        int fallo = 0;
        int[] stock = new int[datosProductos.length];

        // se obtiene el objeto cliente a partir del rut
        for(Cliente cliente : clientes){
            if(cliente.getRut().equals(rutCliente)){
                a = clientes.indexOf(cliente);
                break;
            }
        }
        if(a == -1){
            throw new ClienteNoEncontradoException("No se ha encontrado un cliente con ese rut");
        }

        // revisa si hay stock disponible para los productos
        for(int i = 0; i < datosProductos.length; i++){
            for(Producto producto : productos){
                if(producto.getCodigo() == datosProductos[i][0]){
                    b = productos.indexOf(producto);
                    break;
                }

            }
            if(b ==-1){
                throw new ProductoNoEncontradoException("No existe un Producto con ese codigo");
            }
            try {
                productos.get(b).removeStock(datosProductos[i][1]);
            }catch (ValorInvalidoException e) {
                System.out.println(e.getMessage());
                fallo = -1;
                break;
            }
            stock[i] += datosProductos[i][1];

        }


        //finalmente crea los pedidos, a no ser que existan errores ,en ese caso el stock reservado regresa al stock general
        if(fallo == -1){
            for(int i = 0; i < stock.length; i++){
                try {
                    productos.get(i).addStock(stock[i]);
                } catch (ValorInvalidoException e) {
                    System.out.println(e.getMessage());
                }
            }

        }else{
            for(int[] datos: datosProductos) {
                pedidos.add(new Pedido(pedidos.size(), LocalDate.now() ,clientes.get(a)));
                try {
                    clientes.get(a).addPedido(pedidos.get(pedidos.size()-1));
                }catch (ObjetoRepetidoException | ValorInvalidoException e) {
                    System.out.println(e.getMessage());
                }
                try {
                    lineaPedidos.add(new LineaPedido( pedidos.get(pedidos.size()-1),productos.get(b),datos[1]));
                } catch (ValorInvalidoException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return fallo;
    }
    public void addCliente(String rut,String nombre,String domicilio,String telefono)throws ObjetoRepetidoException{
        for(Cliente cliente:clientes){
            if(cliente.getRut().equals(rut)){
                throw new ObjetoRepetidoException("Ya existe un cliente con ese rut");
            }
        }
        for(Repartidor repartidor:repartidores){
            if(repartidor.getRut().equals(rut)){
                throw new ObjetoRepetidoException("Ya existe un repartidor con ese rut");
            }
        }
        clientes.add(new Cliente(rut, nombre, domicilio, telefono));
    }
    public void addRepartidor(String rut, String nom, String dom, String tel)throws ObjetoRepetidoException{
        for(Repartidor repartidor:repartidores){
            if(repartidor.getRut().equals(rut)){
                throw new ObjetoRepetidoException("Ya existe un repartidor con ese rut");
            }
        }
        for(Cliente cliente:clientes){
            if(cliente.getRut().equals(rut)){
                throw new ObjetoRepetidoException("Ya existe un cliente con ese rut");
            }
        }
        repartidores.add(new Repartidor(rut, nom, dom, tel));
    }
    public void addProductoBasico(int cod, String nombre, int precioCosto, int margenVenta )throws ObjetoRepetidoException, ValorInvalidoException{
        for(Producto producto:productos){
            if(producto.getCodigo() == cod){
                throw new ObjetoRepetidoException("Ya existe un producto con ese codigo");
            }
        }
        if(precioCosto <= 0){
            throw new ValorInvalidoException("El precio de venta del pedido es menor o igual que 0");
        }
        if(margenVenta <= 0){
            throw new ValorInvalidoException("El margen de venta del pedido es menor o igual que 0");
        }
        if(margenVenta > 100){
            throw new ValorInvalidoException("El margen de venta del pedido es mayor que 100");
        }
        productos.add(new ProductoBasico(cod, nombre, precioCosto, margenVenta));
    }
    public void addCombo(int codigo, String nombre, int margenVenta) throws ObjetoRepetidoException, ValorInvalidoException {
        for(Producto producto: productos){
            if(producto.getCodigo() == codigo){
                throw new ObjetoRepetidoException("Ya existe un producto con ese codigo");
            }
        }
        if(margenVenta <= 0){
            throw new ValorInvalidoException("El margen de venta del pedido es menor o igual que 0");
        }
        if(margenVenta > 100){
            throw new ValorInvalidoException("El margen de venta del pedido es mayor que 100");
        }
        productos.add(new Combo(codigo,nombre,margenVenta));
    }
    public void addStockAProducto(int codigo, int nroUnidades)throws ProductoNoEncontradoException,ValorInvalidoException{
        boolean a = false;
        if(nroUnidades <= 0){
            throw new ValorInvalidoException("El numero de unidades a añadir es menor o igual a 0");
        }
        for(Producto producto:productos){
            if(producto.getCodigo()== codigo){
                try{
                    producto.addStock(nroUnidades);
                } catch (ValorInvalidoException e) {
                    System.out.println(e.getMessage());
                }
                a = true;
                break;
            }
        }
        if(!a){
            throw new ProductoNoEncontradoException("No existe un producto con dicho codigo");

        }
    }
    public void setRepartidorAPedido(String rut, int cod)throws RepartidorNoEncontradoException,PedidoNoEncontradoException {
        int a = -1;
        int b = -1;
        for(Repartidor repartidor:repartidores){
            if(repartidor.getRut().equals(rut)){
                a = repartidores.indexOf(repartidor);

            }

        }
        for (Pedido pedido:pedidos){
            if(pedido.getNumero()==cod){
                b = pedidos.indexOf(pedido);
            }
        }
        if( a == -1){
            throw new RepartidorNoEncontradoException("No existe un repartidor con dicho rut");
        }
        if(b == -1){
            throw new PedidoNoEncontradoException("No existe un pedido con dicho rut");
        }
        pedidos.get(b).setrepartidor((repartidores.get(a)));
        try{
            repartidores.get(a).addPedido(pedidos.get(b));
        } catch (ObjetoRepetidoException | ValorInvalidoException e) {
            System.out.println(e.getMessage());
        }
    }
    public void setEntregaPedido(int cod)throws OperacionInvalidaException, PedidoNoEncontradoException{
        for(Pedido pedido:pedidos){
            if(pedido.getNumero() == cod){
                if(pedido.getEstado() == Pedido.Estado.ENTREGADO){
                    throw new OperacionInvalidaException("El pedido ya fue entregado");
                }
                pedido.setEntregado();
                break;
            }
        }
        throw new PedidoNoEncontradoException("No existe un pedido con dicho codigo");
    }
    public String[][] listProductos() {
        String[][] a = new String[productos.size()][5];
        Scanner b;
        for (int i = 0; i < productos.size(); i++) {
            b = new Scanner(productos.toString());
            b.useDelimiter("/");
            a[i][0] =b.next().replace("[","");
            a[i][1] =b.next();
            a[i][2] =b.next();
            a[i][3] = String.valueOf(productos.get(i).getPrecioVenta());
            a[i][4] =b.next().replace("]","");
        }
        return a;
    }
    public String[][] listRepartidores(){
        String[][] a = new String [repartidores.size()][4];
        Scanner b;

        for(int i= 0; i < repartidores.size(); i++){
            b = new Scanner(repartidores.toString());
            b.useDelimiter("/");
            a[i][0] =b.next().replace("[","");
            a[i][1] =b.next();
            a[i][2] =b.next();
            a[i][3] =b.next().replace("]","");

        }
        return a;
    }
    public String[][] listPedidosPendientes(String rut)throws ClienteNoEncontradoException{
        String[][] a;
        int c = -1;

        for(Cliente cliente: clientes){
            if(cliente.getRut().equals(rut)){
                c = clientes.indexOf(cliente);
            }
        }
        if(c == -1){
            throw new ClienteNoEncontradoException("no existe un cliente con dicho rut");
        }
        a = new String [clientes.get(c).getPedidosPendientes().length] [5];
        for(int i = 0; i < clientes.get(c).getPedidosPendientes().length; i++){


            a[i][0] = String.valueOf(clientes.get(c).getPedidosPendientes()[i].getNumero());
            a[i][1] = clientes.get(c).getPedidosPendientes()[i].getFecha().toString();
            a[i][2] = clientes.get(c).getPedidosPendientes()[i].getEstado().name();
            a[i][3] = clientes.get(c).getPedidosPendientes()[i].getCliente().getRut();
            if(clientes.get(c).getPedidosPendientes()[i].getRepartidor() !=null){
                a[i][4] = clientes.get(c).getPedidosPendientes()[i].getRepartidor().getRut();}



        }
        return a;
    }
    public String[][] listPedidosPorEntregar(String rutRepartidor)throws RepartidorNoEncontradoException{
        String[][] a;
        Scanner b;
        int c = -1;
        for (int i = 0; i < repartidores.size(); i++){
            if(repartidores.get(i).getRut().equals(rutRepartidor)){
                c = i;
                break;
            }
        }
        if(c == -1){
            throw new RepartidorNoEncontradoException("No existe un repartidor con dicho rut");
        }
        a = new String [repartidores.get(c).getPedidosPendientes().length] [5];
        b = new Scanner(repartidores.get(c).toString());
        b.useDelimiter("/");
        for(int i = 0; i < repartidores.get(c).getPedidosPendientes().length; i++){
            a[i][0] = b.next().replace("[","");
            a[i][1] = b.next();
            a[i][2] = b.next();
            a[i][3] = String.valueOf(productos.get(i).getPrecioVenta());
            a[i][4] = b.next().replace("]","");

        }
        return a;
    }
    public String[][] listProductosMasSolicitados(int nroPedidosMin)throws ValorInvalidoException{
        if(nroPedidosMin <= 1){
            throw new ValorInvalidoException(" El numero de pedidos minimos a listar es menor que 1");
        }
        int[] a = new int[productos.size()];
        int[] b = new int[3];
        Scanner c;
        boolean[] d = new boolean[productos.size()];
        int e= 0;
        String[][] f;
        for (LineaPedido lineaPedido : lineaPedidos) {
            c = new Scanner(lineaPedido.toString());
            c.useDelimiter("/");
            b[0] = c.nextInt();
            b[1] = c.nextInt();
            b[2] = c.nextInt();
            c.close();
            a[b[2]] += b[0];
        }
        for(int i = 0; i < productos.size(); i++){
            if(a[i]>=nroPedidosMin){
                d[i] = true;
                e++;
            }
        }
        f = new String[e][5];
        for(int i= 0; i < e;i++){
            if(d[i]){
                c =  new Scanner(productos.get(i).toString());
                c.useDelimiter("/");
                f[i][0] = c.next().replace("[","");
                f[i][1] = c.next();
                f[i][2] = c.next();
                f[i][3] = c.next();
                f[i][4] = c.next().replace("]","");

                c.close();
            }

        }
        return f;
    }
    public String[] getDatosCliente(String rut)throws ClienteNoEncontradoException{
        String[] a = new String[4];
        Scanner b;
        int c = -1;
        for(Cliente cliente:clientes){
            if(cliente.getRut().equals(rut)){
                c = clientes.indexOf(cliente);
                break;
            }
        }
        if(c == -1){
            throw new ClienteNoEncontradoException("No existe un cliente con dicho rut");
        }
        b = new Scanner(clientes.get(c).toString());
        b.useDelimiter("/");
        a[0] = b.next().replace("[","");
        a[1] = b.next();
        a[2] = b.next();
        a[3] = b.next().replace("]","");

        return a;
    }
    public String[] getDatosProducto(int codigo)throws ProductoNoEncontradoException{
        String[] a = new String[5];
        Scanner b;
        int c = -1;

        for(Producto producto:productos){
            if(producto.getCodigo() == codigo){
                c = productos.indexOf(producto);
                break;
            }
        }
        if(c == -1){
            throw new ProductoNoEncontradoException("No existe un producto con dicho codigo");
        }
        b = new Scanner(productos.get(c).toString());
        b.useDelimiter("/");
        a[0] = b.next().replace("[","");
        a[1] = b.next();
        a[2] = b.next();
        a[3] = String.valueOf(productos.get(c).getPrecioVenta());
        a[4] = b.next().replace("]","");
        return a;
    }
    public String[] getDatosRepartidor(String rut)throws RepartidorNoEncontradoException{
        String[] a = new String[4];
        Scanner b;
        int c = -1;

        for(Repartidor repartidor :repartidores){
            if(repartidor.getRut().equals(rut)){
                c = repartidores.indexOf(repartidor);
                break;
            }
        }
        if(c == -1){
            throw new RepartidorNoEncontradoException("No existe un repartidor con dicho rut");
        }
        b = new Scanner(repartidores.get(c).toString());
        b.useDelimiter("/");
        a[0] = b.next().replace("[","");
        a[1] = b.next();
        a[2] = b.next();
        a[3] = b.next().replace("]","");
        return a;
    }
    public void readDatosSistema(){

        pedidos.clear();
        productos.clear();
        clientes.clear();
        repartidores.clear();
        lineaPedidos.clear();

        try {
            Collections.addAll(pedidos,IOPedidosRestoran.getInstance().readPedidos());
            System.out.println("Pedidos cargados correctamente");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("La lectura de pedidos ha fallado");
        }
        try {
            Collections.addAll(productos,IOPedidosRestoran.getInstance().readProductosSinPedido());
            productos.toArray(IOPedidosRestoran.getInstance().readProductosSinPedido());
            System.out.println("Productos cargados correctamente");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("La lectura de productos ha fallado");
        }
        try {
            Collections.addAll(clientes,IOPedidosRestoran.getInstance().readClientesSinPedido());
            clientes.toArray(IOPedidosRestoran.getInstance().readClientesSinPedido());
            System.out.println("Clientes cargados correctamente");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("La lectura de clientes ha fallado");
        }
        try {
            Collections.addAll(repartidores, IOPedidosRestoran.getInstance().readRepartidoresSinPedido());
            System.out.println("Repartidores cargados correctamente");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("La lectura de repartidores ha fallado");
        }

        for(Pedido pedido: pedidos){
            clientes.add(pedido.getCliente());
            repartidores.add(pedido.getRepartidor());

            boolean fallo;
            for(LineaPedido lineaPedido :pedido.getLineasPedido()){
                lineaPedidos.add(lineaPedido);
                fallo = false;
                for(Producto producto :productos){
                    if(producto.getCodigo() == lineaPedido.getProducto().getCodigo()){
                        fallo = true;
                        break;
                    }
                }
                if(!fallo){
                    productos.add(lineaPedido.getProducto());
                }
            }
        }

    }
    public void writeDatosSistema() throws IOException {
        IOPedidosRestoran.getInstance().saveProductosYPersonasSinPedidos(productos.toArray(new Producto[0]),clientes.toArray(new Cliente[0]),repartidores.toArray(new Repartidor[0]) );
        IOPedidosRestoran.getInstance().savePedidos(pedidos.toArray(new Pedido[0]));
    }
}
